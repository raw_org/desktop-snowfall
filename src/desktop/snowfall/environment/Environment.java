/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.environment;

import desktop.snowfall.Exceptions.WeatherArgumentException;
import java.io.Serializable;
import java.util.Map;

/**
 * @author robert.worrall@r-a-w.org
 * An abstract class that holds all the data requred to create the weather. 
 */
public abstract class Environment implements Serializable{
    private static final long serialVersionUID = 2261L;
    
    public abstract Integer getwindowHeight();
    public abstract Integer getwindowWidth();
    public abstract void setTheAmountOfWeatherObjects(Integer numberOfObject);
    public abstract Integer getTheAmountOfWeatherObjects();
    public abstract void setFallTime(Integer falltime);
    public abstract Integer getFallTime();
    public abstract void setMaxSizeOfObject(Integer maxSizeOfObject);
    public abstract Integer getMaxSizeOfObject();
    
    /**
     * 
     * @return Map representing the mix of objects in the environment. Where String is the class name and Integer is the percentage incidence of the item.
     */
    public abstract Map<String, Integer> getWeatherMix();
    
    public abstract void addWeatherObject(String canonicalName, Integer occuranceAsAPercentage)throws WeatherArgumentException;
    public abstract void amendWeatherObject(String canonicalName, Integer occranceAsAPercentage) throws WeatherArgumentException;
    public abstract void removeWeatherObject(String canonicalName);
    
}
