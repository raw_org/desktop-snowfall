/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package desktop.snowfall.environment;

import desktop.snowfall.Exceptions.WeatherArgumentException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *@author robert.worrall@r-a-w.org
 * Implementation of the Environment class that holds the information to create the weather
 */
public class EnvironmentImpl extends Environment{

    private Integer numberOfObjects;
    private Integer falltime;
    private Integer maxSizeOfObject;
    private Map<String, Integer> weatherObjects;

    public EnvironmentImpl() {
        weatherObjects = new HashMap<>();
        }

    
    /**
     * 
     * @param numberOfObjects Integer representing the number of objects in the environment
     * @param falltime Integer representing the time it takes for a weather object to fall down the screen
     * @param maxSizeOfObject Integer representing the maximum size of a weather object. 
     */
    public EnvironmentImpl(Integer numberOfObjects, Integer falltime, Integer maxSizeOfObject) {
        this();
        this.numberOfObjects = numberOfObjects;
        this.falltime = falltime;
        this.maxSizeOfObject = maxSizeOfObject;   
    }
    
    
/**
 * 
     * @return Integer that represents the height of the screen by using the swing library.
 */
    @Override
    public Integer getwindowHeight() {
        GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        return Arrays.asList(screenDevices).stream()
                .map(GraphicsDevice::getDefaultConfiguration)
                .map(GraphicsConfiguration::getBounds)
                .map(Rectangle::getHeight).mapToInt(Double::intValue).sum();     
    }

    /**
     * 
     * @return Integer that represents the width of the screen by using the swing library.
     */
    @Override
    public Integer getwindowWidth() {
        GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        return Arrays.asList(screenDevices).stream()
                .map(GraphicsDevice::getDefaultConfiguration)
                .map(GraphicsConfiguration::getBounds)
                .map(Rectangle::getWidth).mapToInt(Double::intValue).sum();     
    }

    
    /**
     * 
     * @param numberOfObjects Integer that represents the number of weather objects in environment
     */
    @Override
    public void setTheAmountOfWeatherObjects(Integer numberOfObjects) {
        this.numberOfObjects = numberOfObjects;
    }

    /**
     * @return Integer representing the number of weather objects in the environment
     */
    @Override
    public Integer getTheAmountOfWeatherObjects() {
        return numberOfObjects;
    }

    /**
     * @param falltime Integer that represents the time it takes for a weather object to fall down the screen
     */
    @Override
    public void setFallTime(Integer falltime) {
        this.falltime = falltime;
    }

    /**
     * 
     * @return Integer that represents the time it takes for a weather object to fall down the screen
     */
    @Override
    public Integer getFallTime() {
        return falltime;
    }

    /**
     * 
     * @param maxSizeOfObject Integer that represents the maximum size of a weather object
     */
    @Override
    public void setMaxSizeOfObject(Integer maxSizeOfObject) {
        this.maxSizeOfObject = maxSizeOfObject;
    }

    /**
     * @return Integer that represents the maximum size of a weather object
     */
    @Override
    public Integer getMaxSizeOfObject() {
       return maxSizeOfObject;
    }

    /**
     * 
     * @return map made up of Key: String Vale (class name of weather object to be created) and Value: Integer percentage of total number of objects to created in a scene 
     */
    @Override
    public Map<String, Integer> getWeatherMix() {
        return weatherObjects;
    }

    /**
    *
     * @param canonicalName 
     * @param occuranceAsAPercentage 
     * @throws desktop.snowfall.Exceptions.WeatherArgumentException
    */
    @Override
    public void addWeatherObject(String canonicalName, Integer occuranceAsAPercentage) throws WeatherArgumentException {
        if(checkWeatherOccuranceArguement(occuranceAsAPercentage))
        weatherObjects.put(canonicalName, occuranceAsAPercentage);
        else{
        throw new WeatherArgumentException("incorrect occurance for object" + canonicalName);
        }
    }

    /**
     * 
     * @param canonicalName String Vale (class name of weather object to be created) 
     * @param occranceAsAPercentage Integer percentage of total number of objects to created in a scene 
     * @throws desktop.snowfall.Exceptions.WeatherArgumentException 
     */
    @Override
    public void amendWeatherObject(String canonicalName, Integer occranceAsAPercentage) throws WeatherArgumentException{
        if(checkWeatherOccuranceArguement(occranceAsAPercentage))
        weatherObjects.replace(canonicalName, occranceAsAPercentage);
        else{
           throw new WeatherArgumentException("incorrect occurance for object" + canonicalName);
}
    }
    
/**
 * Checks the if the weather argument is correct.
 */    
private boolean checkWeatherOccuranceArguement(Integer occuranceAsAPercentage) {
    
    System.out.println("Testing Value: " + occuranceAsAPercentage);
    if (occuranceAsAPercentage > 101)
        return false;
    else
        System.out.println("Total weather objects before being added" + weatherObjects.values().stream().mapToInt(Integer::intValue).sum());
       return 101 > (occuranceAsAPercentage + (weatherObjects.values().stream().mapToInt(Integer::intValue).sum()));
    //System.out.println("Test argument: " + validArgument);
               
}
    /**
     * 
     * @param canonicalName String representing the name of the class to be be removed.
     */
    @Override
    public void removeWeatherObject(String canonicalName) {
        weatherObjects.remove(canonicalName);
    }
    
    


}
   