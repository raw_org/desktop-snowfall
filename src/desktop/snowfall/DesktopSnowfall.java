/*
 *
 */
package desktop.snowfall;

import desktop.snowfall.Exceptions.WeatherArgumentException;
import desktop.snowfall.weather.Snowfall;
import desktop.snowfall.Exceptions.WeatherCreationException;
import desktop.snowfall.environment.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import static javafx.application.Platform.exit;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Robert.Worrall@r-a-w.org
 */
public class DesktopSnowfall extends Application {

    private Snowfall snow;
    private Environment env;
   

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        getSettings("desktop.snowfall.environment.EnvironmentImpl");
        new SnowSettingsDialog(env).showAndWait(); 
        saveSettings();
        createTheWeather();
        configureThePrimaryStage(primaryStage);
        setTheStage(primaryStage);
        snow.startSnowing();
    }

    private void setTheStage(Stage primaryStage) {
        Scene aScene = new Scene(createAndConfigureRootPane(), env.getwindowWidth(), env.getwindowHeight());
        aScene.setFill(Color.TRANSPARENT);
        primaryStage.setScene(aScene);
        primaryStage.show();
    }

    private Pane createAndConfigureRootPane() {
        Pane root = new Pane();
        root.setStyle("-fx-background-color: transparent;");
        root.getChildren().addAll(snow);
        return root;
    }

    private void configureThePrimaryStage(Stage primaryStage) {
        //ensure it is always on top
        primaryStage.setAlwaysOnTop(true);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setX(0);
        primaryStage.setY(0);
        primaryStage.setTitle("Snowfall");
        primaryStage.setWidth(env.getwindowWidth());
        primaryStage.setHeight(env.getwindowHeight());
    }

    private void createTheWeather() {
        try {
            //create the snowfall
            snow = new Snowfall(env, "desktop.snowfall.weather.factories.SimpleWeatherFactory");
        } catch (WeatherCreationException ex) {
            Logger.getLogger(DesktopSnowfall.class.getName()).log(Level.SEVERE, null, ex);
            exit();
        }
    }

    private void getSettings(String environmentClass) {
        try {
            FileInputStream fileInput = new FileInputStream(new File("weatherENV.ds"));
            ObjectInputStream input = new ObjectInputStream(fileInput);
            env = (Environment) input.readObject();
            
            input.close();
            fileInput.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Unable to retrieve settigns using default settings" + ex);
            try {
                env = (Environment) Class.forName(environmentClass).newInstance();
                env.setFallTime(10);
                env.setTheAmountOfWeatherObjects(300);
                env.setMaxSizeOfObject(10);
                env.addWeatherObject("desktop.snowfall.weather.Snowflake_blurred", 100);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | WeatherArgumentException creationException) {
                System.err.println("problem with weatherObjects"  + creationException);
            } 
        }

    }

    void saveSettings() {
        try {
            try (FileOutputStream fileOutputStream = new FileOutputStream(new File("weatherENV.ds")); 
                    ObjectOutputStream output = new ObjectOutputStream(fileOutputStream)) {
                output.writeObject(env);
            }
        } catch (IOException ex) {
            System.err.println("Unable to save settings file: " + ex);
        }
    }
}
