package desktop.snowfall;

import desktop.snowfall.environment.Environment;
import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Robert Worrall
 * Settings dialog box that shows the settings for the snowfall, initially for 1 seconds then the dialog closes.
 */
public class SnowSettingsDialog {

    private Environment environment;
    private int countDown = 10;
    private int scaleX = 3;
    private int scaleY = 3;
    private StackPane countDownIcon;
    private Text countDownIndicator;
    private final Stage settingAlert = new Stage();
    private FadeTransition fadeCountdown_down;
    private FadeTransition fadeCountdown_up;
    private SequentialTransition countTransition;
    private boolean pause = false;

    public SnowSettingsDialog(Environment environment) {
        this.environment = environment;
        createCountDownIcon();
        putPanelsTogether(configureRoot(), createOptionsPane(), createCountPane());
    }

    private FlowPane createOptionsPane() {
        FlowPane options = new FlowPane();//where options such as start, wait sit.
        options.setBlendMode(null);
        initaliseOptionsPane(options);
        return options;
    }

    private VBox createCountPane() {
        VBox countpane = new VBox();//where the countdown sits.
        countpane.setBlendMode(null);
        countpane.getChildren().addAll(new Text("Snowfall Starting in.."), countDownIcon, createPauseButton());
        return countpane;
    }

    private void setTheScene(BorderPane root) {
        Scene settingScene = new Scene(root, 450, 170);
        settingScene.setFill(Color.TRANSPARENT);
        settingAlert.setScene(settingScene);
        settingAlert.setTitle("Welcome to Snowfall");
    }

    private void putPanelsTogether(BorderPane root, FlowPane options, VBox countpane) {
        root.setCenter(initaliseSettingsPane());
        root.setBottom(options);
        root.setRight(countpane);
        setTheScene(root);
    }

    private Button createPauseButton() {  
        Button pauseCountDown = new Button("Pause");
        pauseCountDown.setOnAction((ActionEvent event) -> {
            if (pause == false) {
                pause = true;
                countTransition.pause();
                pauseCountDown.setText("Paused");
            } else {
                pause = false;
                countTransition.play();
                pauseCountDown.setText("Pause");
            }
        });
        pauseCountDown.translateXProperty().set(40);
        pauseCountDown.translateYProperty().set(40);
        return pauseCountDown;
    }

    private BorderPane configureRoot() {
        BorderPane root = new BorderPane();
        root.setBlendMode(null);
        root.setStyle("-fx-background-color: transparent;");
        return root;
    }

    public int getFalltime() {
        return environment.getFallTime();
    }

    public void setFalltime(int falltime) {
        environment.setFallTime(falltime);
    }

    public int getAmountOfSnow() {
        return environment.getTheAmountOfWeatherObjects();
    }

    public void setAmountOfSnow(int amountOfSnow) {
        environment.setMaxSizeOfObject(amountOfSnow);
    }

    public int getMaximumFlakeSize() {
        return environment.getMaxSizeOfObject();
    }

    public void setMaximumFlakeSize(int maximumFlakeSize) {
        environment.setMaxSizeOfObject(maximumFlakeSize);
    }

    public int getCountDown() {
        return countDown;
    }

    public void setCountDown(int countDown) {
        this.countDown = countDown;
    }

    public void showAndWait() {
        startCountdown();
        settingAlert.showAndWait();
    }

    private void createCountDownIcon() {
        countDownIcon = new StackPane();
        Circle aCircle = new Circle(10, Color.WHITE);
        aCircle.setStroke(Color.BLACK);
        aCircle.getStrokeDashArray().addAll(5d, 2d, 5d, 2d);
        aCircle.setStrokeWidth(1.4);
        countDownIndicator = new Text("" + countDown);
        countDownIcon.setScaleX(scaleX);
        countDownIcon.setScaleY(scaleY);
        countDownIcon.getChildren().addAll(aCircle, countDownIndicator);
        countDownIcon.setTranslateX(10);
        countDownIcon.setTranslateY(30);
        createTransitions();
    }

    private void createTransitions() {
        fadeCountdown_down = new FadeTransition();
        fadeCountdown_down.setNode(countDownIndicator);
        fadeCountdown_down.setAutoReverse(false);
        fadeCountdown_down.setFromValue(1.0);
        fadeCountdown_down.setToValue(0.0);
        fadeCountdown_down.setDuration(Duration.millis(500));
        fadeCountdown_down.setOnFinished(event -> countDownIndicator.setText("" + --countDown));
        fadeCountdown_up = new FadeTransition();
        fadeCountdown_up.setNode(countDownIndicator);
        fadeCountdown_up.setAutoReverse(false);
        fadeCountdown_up.setFromValue(0.0);
        fadeCountdown_up.setToValue(1.0);
        fadeCountdown_up.setDuration(Duration.millis(500));
        fadeCountdown_up.setOnFinished(event -> closeSettingAlert());
        countTransition = new SequentialTransition();
        countTransition.getChildren().addAll(fadeCountdown_down, fadeCountdown_up);
        countTransition.setCycleCount(countDown);
    }

    private void startCountdown() {

        countTransition.play();
    }

    private Pane initaliseSettingsPane() {
        VBox settings = createNewSettingsVBox();
        Text amountofsnowlabel = new Text("Amount of Snow(please enter a number)");
        TextField amountOfSnowField = configureAmountOfSnowLabelAndField(amountofsnowlabel);
        Text fallTimeLabel = new Text("Fall time 'The time it takes the snow to fall down the screen in seconds");
        TextField fallTimeField = configureFallTimeFieldAndLabel(fallTimeLabel);
        Text flakeSizeLabel = new Text("The Maximum size of the snow flakes");
        TextField flakeSizeField = configureMaxSizOfSnowFieldAndLabel(flakeSizeLabel);
        settings.getChildren().addAll(amountofsnowlabel, amountOfSnowField, fallTimeLabel, fallTimeField, flakeSizeLabel, flakeSizeField);
        return settings;
    }

    private VBox createNewSettingsVBox() {
        VBox settings = new VBox();// where fields for settings sit.
        settings.setBlendMode(null);
        settings.setPrefSize(200, 500);
        return settings;
    }

    private TextField configureMaxSizOfSnowFieldAndLabel(Text flakeSizeLabel) {
        TextField flakeSizeField = new TextField();
        flakeSizeField.setText("" + environment.getMaxSizeOfObject());
        flakeSizeField.setBlendMode(null); 
        flakeSizeField.textProperty().addListener((observable, oldValue, value) -> {
            //first check the field contains a number
            try {
                int intvalue = Integer.parseInt(value);
                flakeSizeLabel.setFill(Color.BLACK);
                environment.setMaxSizeOfObject(intvalue);
            } catch (NumberFormatException e) {
                flakeSizeLabel.setFill(Color.RED);
            }
        });
        return flakeSizeField;
    }

    private TextField configureFallTimeFieldAndLabel(Text fallTimeLabel) {
        TextField fallTimeField = new TextField();
        fallTimeField.setText("" + environment.getFallTime());
        fallTimeField.setBlendMode(null);
        fallTimeField.textProperty().addListener((observable, oldValue, value) -> {
            //first check the field contains a number
            try {
                int intvalue = Integer.parseInt(value);
                fallTimeLabel.setFill(Color.BLACK);
                environment.setFallTime(intvalue);
            } catch (NumberFormatException e) {
                fallTimeLabel.setFill(Color.RED);
            }
        });
        return fallTimeField;
    }

    private TextField configureAmountOfSnowLabelAndField(Text amountofsnowlabel) {
        TextField amountOfSnowField = new TextField();
        amountOfSnowField.setText(environment.getTheAmountOfWeatherObjects() + "");
        amountOfSnowField.setBlendMode(null);
        amountOfSnowField.textProperty().addListener((observable, oldValue, value) -> {
            try {
                int intvalue = Integer.parseInt(value);
                amountofsnowlabel.setFill(Color.BLACK);
                environment.setTheAmountOfWeatherObjects(intvalue);
            } catch (NumberFormatException e) {
                amountofsnowlabel.setFill(Color.RED);
            }
        });
        return amountOfSnowField;
    }

    private void initaliseOptionsPane(FlowPane options) {
        //create 2 buttons start & exit
        Button startSnowing = new Button("Start Snowing");
        startSnowing.setOnAction((ActionEvent event) -> {
            settingAlert.close();
        });

        Button exit = new Button("Exit");
        exit.setOnAction((ActionEvent event) -> {
            System.exit(0);
        });
        options.getChildren().addAll(startSnowing, exit);
    }

    private void closeSettingAlert() {
        if (pause == true) {
            //then do not close the settings window
        } else {
            //if the countdown has reached zero then close the settings window
            if (countDown <= 0) {
                settingAlert.close();
            }
        }
    }

}
