/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package desktop.snowfall.Exceptions;

/**
 * @author robert.worrall@r-a-w.org
 * Exception that is thrown when the weatherMix Object given to a weatherFactory is incorrect. 
 */
public class IncorrectWeatherMixException extends Exception {

    /**
     * 
     * @param exeception String representing the reason for the exception
     */
    public IncorrectWeatherMixException(String exeception) {
        
        super("Incorrect Weather Mix" + exeception);
    }

}
