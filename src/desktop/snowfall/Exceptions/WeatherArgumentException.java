/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package desktop.snowfall.Exceptions;

/**
 * @author robert.worrall@r-a-w.org
 * Exception that is thrown when an argument for a weatherObject being added to a WeatherMix variable on a Weather object is incorrect. 
 */
public class WeatherArgumentException extends Exception {

    /**
     *
     * @param error String representing the error that caused the Exception
     */
    public WeatherArgumentException(String error) {
        super(error);
    }

}
