/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package desktop.snowfall.Exceptions;

/**
 * @author robert.worrall@r-a-w.org
 * Exception to be thrown when there are problems creating Weather objects
 */
public class WeatherCreationException extends Exception {

    /**
     * 
     * @param string String representing the reason for the exception.
     */
    public WeatherCreationException(String string) {
        super(string);
    }

    
}
