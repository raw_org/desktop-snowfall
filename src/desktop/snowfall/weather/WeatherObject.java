/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.weather;

/**
 * Interface for WeatherObjects that make up an Weather Object. 
 * @author worralr
 */
public interface WeatherObject{
    
    /**
     * @param size the size of the weatherObject
    */
    public void setSize(Integer size);
    public Integer getSize();
    
    /**
     * This builds a weatherObject ready for displaying on screen
     */
    public void build();
    
}
