package desktop.snowfall.weather;

/*
 * Snowflake_blurred.fx
 *
 * Created on 16-Dec-2009, 10:58:03
 */





import javafx.scene.Group;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;
import javafx.scene.effect.MotionBlur;



/**
 * @author Robert Worrall
 * Implementation of WeatherObject creates a blurred white circle.
 */
public class Snowflake_blurred extends Group implements WeatherObject{

    private Integer size;
    
    /**
     * Standard constructor
     */
    public Snowflake_blurred(){
    super();
    }
        
/**
 * creates an instance of snowflake
     * @param diameter the maximum size of the snowflake
 */
    public Snowflake_blurred(int diameter) {  
        Circle aCircle = new Circle(0,0,diameter,Color.WHITE);
        MotionBlur blur =  new MotionBlur();
        blur.setRadius(15.0f);
        blur.setAngle(45.0f);
        aCircle.setEffect(blur);
        this.getChildren().add(aCircle); 
        this.size = diameter;
    }

    /**
     * 
     * @param maxSize maximum size of the object
     */
    @Override
    public void setSize(Integer maxSize) {
        this.size = maxSize;
    }

    /**
     * Creates the weather object ready for displaying
     */
    @Override
    public void build() {
        Circle aCircle = new Circle(0,0,size,Color.WHITE);
        MotionBlur blur =  new MotionBlur();
        blur.setRadius(15.0f);
        blur.setAngle(45.0f);
        aCircle.setEffect(blur);
        this.getChildren().add(aCircle);
    }

    /**
     * 
     * @return returns the size size of the weatherobject
     */
    @Override
    public Integer getSize() {
        return size;
    }

}
        

        