/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.weather;

import desktop.snowfall.Exceptions.WeatherCreationException;
import desktop.snowfall.environment.Environment;
import desktop.snowfall.weather.factories.WeatherFactory;
import javafx.scene.Group;

/**
 *
 * @author Robert.worrall@r-a-w.org
 * Abstract Class that holds all the weatherObjects and alters their behaviour. e.g makes them fall down the screen. It also contains the factory to create the weatherObjects it is to hold
 */
public abstract class Weather extends Group {

    Environment environment;
    WeatherFactory factory;

    
/**
 * 
     * @param environment this holds features of the weather
     * @param factory the factory to be instantiated for weather object creation
     * @throws desktop.snowfall.Exceptions.WeatherCreationException
 */    
    public Weather(Environment environment, String factory) throws WeatherCreationException {
        this.environment = environment;
        try {
            this.factory = (WeatherFactory) Class.forName(factory).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            throw new WeatherCreationException(factory + " " + ex);
        }
    }

}
