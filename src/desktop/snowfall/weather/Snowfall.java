/*
 * Snowfall.fx
 *
 * Created on 16-Dec-2009, 10:28:54
 */
package desktop.snowfall.weather;

import desktop.snowfall.Exceptions.IncorrectWeatherMixException;
import desktop.snowfall.Exceptions.WeatherCreationException;
import desktop.snowfall.environment.Environment;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.animation.Timeline;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * @author Robert.Worrall@r-a-w.org This creates a group of 8 bands of snow the screen
 * height is spit into seven bands the idea being that once there is a band to
 * take over at the top as one disappears at the bottom.
 */
public class Snowfall extends Weather {


    List<Timeline> timelines;

    
    /**
     * 
     * @param environment 
     * @param weatherFactory
     * @throws desktop.snowfall.Exceptions.WeatherCreationException
     */
    public Snowfall(Environment environment, String weatherFactory) throws WeatherCreationException {
        super(environment, weatherFactory);
        tryToSetFactoryWeatherMix(environment, weatherFactory);
        List<Group> snowbands = generateSnowBands();
        timelines = new ArrayList();
        snowbands.stream().forEach(cnsmr -> timelines.add(animateBand(cnsmr)));
    }

    private List<Group> generateSnowBands() {
        List<Group> snowbands = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            snowbands.add(createSnowBand());
        }
        this.getChildren().addAll(snowbands);
        return snowbands;
    }

    private int getBandSize() {
        return this.environment.getwindowHeight() / 7;
    }

    private void tryToSetFactoryWeatherMix(Environment environment1, String weatherFactory) throws WeatherCreationException {
        try {
            factory.setWeatherMix(environment1.getWeatherMix());
        } catch (IncorrectWeatherMixException ex) {
            throw new WeatherCreationException("Unable create Weather Factory" + weatherFactory + " " + ex);

        }
    }

    /**
     * Method to create a band of snow using 1/7th of the total snow for the
     * scene
     */
    private Group createSnowBand() {
        Group snowGroup = new Group();
        List<Group> weatherGroupList = factory.getWeatherObjects(getSnowInBand(), environment.getMaxSizeOfObject());
        weatherGroupList.stream().forEach(aGroupOfWeatherObjects -> {
            aGroupOfWeatherObjects.getChildren().stream().forEach(
                    aNode -> {configureWeatherObjectPosition(aNode, aGroupOfWeatherObjects, getBandSize());});
            snowGroup.getChildren().add(aGroupOfWeatherObjects);
        });
        return snowGroup;
    }

    private int getSnowInBand() {
        return environment.getTheAmountOfWeatherObjects() / 7;
    }

    private void configureWeatherObjectPosition(Node weatherObject, Group owningGroup, Integer height) {
        if (!weatherObject.getClass().getCanonicalName().equalsIgnoreCase(Group.class.getCanonicalName())) {
            weatherObject.setTranslateX(new Random().nextInt(environment.getwindowWidth()));
            weatherObject.setTranslateY(new Random().nextInt(height));
        } else {
            System.out.println("Group Node do not alter x and y");
        }

    }

    /**
     *
     * method to create the animation for the snowbands it takes a group
     * representing a snowband and an int for the order in which the animation
     * is started at between 1 and 8 for each band of snow.
     * @param aSnowBand Group of weatherObjects in this case expected to be snow like objects
     * @return Timeline animating the weatherObjects down the screen
     */
    public Timeline animateBand(Group aSnowBand) {
        //first create the start time for the animation is order multiplied buy the falltime devided by 8 and half to allow some overlap      
        Timeline timeline = new Timeline();
        timeline.setAutoReverse(false);
        addKeyFrames(timeline, aSnowBand);
        timeline.setCycleCount(Timeline.INDEFINITE);
        return timeline;
    }

    private void addKeyFrames(Timeline timeline, Group aSnowBand) {
        timeline.getKeyFrames().addAll(
                new KeyFrame(Duration.ZERO,// setstarting posistion 
                        new KeyValue(aSnowBand.translateYProperty(), 0 - getBandSize())
                ),
                new KeyFrame(getEndDuration(),
                        new KeyValue(aSnowBand.translateYProperty(), environment.getwindowHeight() + getBandSize())
                )
        );
    }

    private Duration getEndDuration() {
        return new Duration(environment.getFallTime() * 1000);
    }

    public void startSnowing() {
        int count = 1;
        for (Timeline t : timelines) {
            if (count == 1) 
                t.play();  
            t.playFrom(new Duration(((environment.getFallTime() * 1000) / 7) * ++count));
        }  
    }

}
