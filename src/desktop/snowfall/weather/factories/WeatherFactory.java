/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package desktop.snowfall.weather.factories;


import desktop.snowfall.Exceptions.IncorrectWeatherMixException;
import java.util.List;
import java.util.Map;
import javafx.scene.Group;

/**
 *
 * @author Robert.worrall@r-a-w.org
 * Interface for creating weatherFactories that can produce large amount of weatherObjects off a weather mix that is supplied to it.  
 */
public interface WeatherFactory {
    
   /**
     * @param weatherMix Map that represents the mix of weatherObjects to be created. Where String is the canonical class name and Integer is the precentage of the total objects to be created. 
     * @throws desktop.snowfall.Exceptions.IncorrectWeatherMixException
    */
    public void setWeatherMix(Map<String, Integer> weatherMix)throws IncorrectWeatherMixException;
    
    /**
     * @param numberOfObjectsNeeded Integer representing the number of WeatherObjects to be created by the factory
     * @param maxSize Integer representing the maximum size a WeatherObject can be. 
     * @return List of Group Objects, were each Group object is of a different class of WeatherObject. 
     */
   public List<Group> getWeatherObjects(Integer numberOfObjectsNeeded, Integer maxSize);

}
