/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.weather.factories;

import desktop.snowfall.Exceptions.IncorrectWeatherMixException;
import desktop.snowfall.weather.WeatherObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Group;
import javafx.scene.Node;

/**
 * @author Robert.worrall@r-a-w.org
 * Class for creating weatherFactories that can produce large amount of weatherObjects off a weather mix that is supplied to it.  
 */
public class SimpleWeatherFactory implements WeatherFactory {

    private Map<String, Integer> weatherMix;

    /**
     * @param weatherMix Map that represents the mix of weatherObjects to be created. Where String is the canonical class name and Integer is the precentage of the total objects to be created. 
     * @throws desktop.snowfall.Exceptions.IncorrectWeatherMixException
    */
    @Override
    public void setWeatherMix(Map<String, Integer> weatherMix) throws IncorrectWeatherMixException {
        this.weatherMix = weatherMix;
    }

    /**
     * @param numberOfObjectsNeeded Integer representing the number of WeatherObjects to be created by the factory
     * @param maxSize Integer representing the maximum size a WeatherObject can be. 
     * @return List of Group Objects, were each Group object is of a different class of WeatherObject. 
     */
    @Override
    public List getWeatherObjects(Integer numberOfObjectsNeeded, Integer maxSize) {
        ArrayList<Group> returnGroup = new ArrayList<>();
        weatherMix.entrySet().forEach((entry) -> {
            returnGroup.add(createWeatherObjects(entry.getKey(), maxSize, entry.getValue(), numberOfObjectsNeeded));
        });
        return returnGroup;
    }

    private Group createWeatherObjects(String className, Integer maxSize, Integer percentage, Integer totalnumberOfObjects) {
        Group returnGroup = new Group();
        for (int count = 1; count <= numberOfObjectsNeeded(percentage, totalnumberOfObjects); count++) {
            try {
                WeatherObject weatherObject = constructObject(className, maxSize);
                returnGroup.getChildren().add((Node) weatherObject);
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                Logger.getLogger(SimpleWeatherFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return returnGroup;
    }

    private WeatherObject constructObject(String className, Integer maxSize) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        WeatherObject weatherObject = (WeatherObject) Class.forName(className).newInstance();
        weatherObject.setSize(new Random().nextInt(maxSize));
        weatherObject.build();
        return weatherObject;
    }

    private double numberOfObjectsNeeded(Integer percentage, Integer totalnumberOfObjects) {
        double numberofobjects = (double) ((double) percentage / 100) * (double) totalnumberOfObjects;
        return numberofobjects;
    }
}
