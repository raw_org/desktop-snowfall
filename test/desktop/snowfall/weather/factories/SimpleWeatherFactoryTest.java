/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.weather.factories;

import desktop.snowfall.weather.Snowflake_blurred;
import java.util.HashMap;
import java.util.List;
import javafx.scene.Group;
import javafx.scene.Node;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author worralr
 */
public class SimpleWeatherFactoryTest {

    private SimpleWeatherFactory instance;
    
    @Before
    public void setup(){
    instance = new SimpleWeatherFactory();
    }

    @Test
    public void shouldReturnWeatherObjectsBasedOnMap() throws Exception{
        HashMap<String, Integer> weatherMap = new HashMap<>();
        weatherMap.put("desktop.snowfall.weather.Snowflake", 20);
        instance.setWeatherMix(weatherMap);
        List<Group> weatherObjects = instance.getWeatherObjects(100,20);
        int objectCount = countObjects(weatherObjects, "desktop.snowfall.weather.Snowflake");
        
        //need to get the group of objects from the list.
        assertThat(objectCount, equalTo(20));        
    }
    
@Test
    public void shouldCreate2SnowflakesWithSizeBetween0and20() throws Exception{
    HashMap<String, Integer> weatherMap = new HashMap<>();
        weatherMap.put("desktop.snowfall.weather.Snowflake", 2);
        instance.setWeatherMix(weatherMap);
        List<Group> weatherObjects = instance.getWeatherObjects(100,20);
        int objectCount = countObjects(weatherObjects, "desktop.snowfall.weather.Snowflake");
        assertThat(objectCount, equalTo(2));  
        Group group =  weatherObjects.get(0);
        Snowflake_blurred flake1 = (Snowflake_blurred) group.getChildren().get(0);
        Snowflake_blurred flake2 = (Snowflake_blurred) group.getChildren().get(1);
        assertTrue(flake1.getSize() <= 20);
        assertTrue(flake1.getSize() > 0);
        assertTrue(flake2.getSize() <= 20);
        assertTrue(flake2.getSize() > 0);
        
    }
    
    
    private int countObjects(List<Group> weatherObjects, String objectClassName) {
        int count = 0;
        for(Group groupOfWeatherObjects : weatherObjects){
            for( Node aNode : groupOfWeatherObjects.getChildren()){
            if(aNode.getClass().getCanonicalName().equalsIgnoreCase(objectClassName))
                count++;
            }

        }
        return count;
    }
    
    
    
}
