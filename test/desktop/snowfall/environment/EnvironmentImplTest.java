/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktop.snowfall.environment;

import desktop.snowfall.Exceptions.WeatherArgumentException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Map;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author worralr
 */
public class EnvironmentImplTest {

    private EnvironmentImpl instance;
    
    @Before
    public void setup(){
    instance = new EnvironmentImpl();
    }

    /**
     * Test of getwindowHeight method, of class EnvironmentImpl.
     */
    @Test
    public void testGetwindowHeight() {        
      
        GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        int height = Arrays.asList(screenDevices).stream()
                .map(GraphicsDevice::getDefaultConfiguration)
                .map(GraphicsConfiguration::getBounds)
                .map(Rectangle::getHeight).mapToInt(Double::intValue).sum();     
        assertThat(height, equalTo(instance.getwindowHeight()));
    }

    /**
     * Test of getwindowWidth method, of class EnvironmentImpl.
     */
    @Test
    public void testGetwindowWidth() {
        
        GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        int width =  Arrays.asList(screenDevices).stream()
                .map(GraphicsDevice::getDefaultConfiguration)
                .map(GraphicsConfiguration::getBounds)
                .map(Rectangle::getWidth).mapToInt(Double::intValue).sum();  
       
        assertThat(width, equalTo(instance.getwindowWidth()));
    }


    /**
     * Test of getWeatherMix, addWeatherObject and removeWeatherObject methods, of class EnvironmentImpl.
     */
    @Test
    public void testGetWeatherMixAddWeatherObjectAndRemoveWeatherObject() throws Exception {
        String canonicalName = "desktop.snowfall.snowflake";
        instance.addWeatherObject(canonicalName, 10);
        Map<String, Integer> weatherMix = instance.getWeatherMix();
        boolean containsCanonicalName = weatherMix.containsKey(canonicalName);
        Integer percent = weatherMix.get(canonicalName);
        instance.removeWeatherObject(canonicalName);
        weatherMix = instance.getWeatherMix();
        boolean doesNotcontainCanonicalName = !weatherMix.containsKey(canonicalName);
        assertTrue(containsCanonicalName);
        assertThat(10, equalTo(percent));
        assertTrue(doesNotcontainCanonicalName);
    }
    
   @Test (expected = WeatherArgumentException.class)
   public void shouldThrowExceptionWhenAddWeatherObjectIsGivenAnIntMoreThan100()throws Exception{
   String canonicalName = "desktop.snowfall.snowflake";
        instance.addWeatherObject(canonicalName, 101);
   }
    
    @Test (expected = WeatherArgumentException.class)
   public void shouldThrowExceptionWhenAddWeatherObjectsGivenArgumentsThatAddUpToMoreThan100()throws Exception{
   String canonicalName = "desktop.snowfall.snowflake";
        instance.addWeatherObject(canonicalName, 10);
        instance.addWeatherObject("leaf", 60);
        instance.addWeatherObject("hale", 50);
   }
   
   @Test
   public void shouldAmendWeatherObject() throws Exception{
   String testName = "Ice";
   instance.addWeatherObject(testName, 70);
   instance.amendWeatherObject(testName, 20);
   Integer updatedValue = instance.getWeatherMix().get(testName);
   assertThat(updatedValue, equalTo(20));
   }
}
